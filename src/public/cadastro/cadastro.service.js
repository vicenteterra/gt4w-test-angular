(function() {
    'use strict';

    angular.module('public').service('cadastroService', cadastroService);

    cadastroService.$inject = ['$http'];

    function cadastroService($http) {
        var service = this;

        service.getUF = function() {
            return $http.get('http://www.geonames.org/childrenJSON?geonameId=3469034')
                .then(function(response) {
                    return response.data;
                });
        }

        service.save = function(user) {
            return $http.post('http://ec2-177-71-200-27.sa-east-1.compute.amazonaws.com:9000/usuario/new', user) 
                .then(function(response) {
                    return response.data;
                });
        }
    }

})();