(function() {
    'use strict';

    angular.module('public')
        .controller('cadastroController', cadastroController);

    cadastroController.$inject = ['$scope', 'cadastroService'];

    function cadastroController($scope, cadastroService) {
        var vm = this;
        vm.novoUsuario = {};
        vm.ufSelected = {};
        vm.ufList = [];

        vm.loadUfs = function() {
            cadastroService.getUF().then(function(response) {
                vm.ufList = response.geonames;
            });
        }
        vm.loadUfs();

        vm.save = function() {
        		if(vm.ufSelected){
        			 vm.novoUsuario.ufID = vm.ufSelected;
        		}
           
            if (vm.novoUsuario.bdate != null) {
                vm.novoUsuario.bdateUnix = vm.novoUsuario.bdate.getTime();
            } 
            cadastroService.save(vm.novoUsuario).then(
                function(response) {
                    if (response.status == 0) {
                        var notify = {
                            type: 'success',
                            title: response.message,
                            content: '',
                            timeout: 5000
                        };
                        $scope.$emit('notify', notify);
                    } else {
                        var notify = {
                            type: 'error',
                            title: response.message,
                            content: '',
                            timeout: 8000
                        };
                        $scope.$emit('notify', notify);
                    }
                });
        }


    }

})();