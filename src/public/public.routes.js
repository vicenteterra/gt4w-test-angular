(function() {
    'use strict';

    angular.module('public').config(routeConfig);


    routeConfig.$inject = ['$stateProvider', '$locationProvider'];

    function routeConfig($stateProvider, $locationProvider) {

        $stateProvider
            .state('cadastro', {
                url: '/cadastro',
                templateUrl: 'src/public/cadastro/cadastro.html',
                controller: 'cadastroController',
                controllerAs: 'cadastroCtrl'
            });
    }
})();